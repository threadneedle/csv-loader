const _ = require('lodash')
const yargs = require('yargs')
const fs = require('fs')
const parseCsv = require('csv-parse/lib/sync')
const fetch = require('node-fetch')

// Extract arguments from the command line
const argv = yargs
    .usage('Usage: node index.js <command> [arguments...]')
    .command('load', 'Convert headers of a CSV file to a Solidatus model', yargs => {
    return yargs
        .option('in', {
            demandOption: true,
            describe: 'Input CSV file',
            type: 'string'
        })
        .option('host', {
            demandOption: true,
            describe: 'URL of solidatus instance',
            type: 'string'
        })
        .option('token', {
            demandOption: true,
            describe: 'Solidatus API token',
            type: 'string'
        })
        .option('model', {
            demandOption: true,
            describe: 'Name of new model or existing model',
            type: 'string'
        })
      })
      .example('$0 load --in file.csv --token <token> --model "My new model"')
    .demandCommand()
    .help().argv

// Read the input csv file
const csv = fs.readFileSync(argv.in)
// Parse the csv file and take the first row which is the headers.
const csvHeaders = parseCsv(csv)[0]

// Build a JavaScript object to hold the entities of our model with an entity for the layer and entity for the object
entities = {
    'layer': {
        children: ['object'],
        name: 'Files'
    },
    'object': {
        children: [],
        name: argv.in
    }
}

var i = 0
// Add each header from the csv headers to the entity object as an entity
for (var column of csvHeaders) {
    if (column == ""){
      continue
    }
    id = i.toString()
    entities[id] = {
        name: column
    }
    entities.object.children.push(id)
    i += 1
}

// Create a JavaScript object containing the entities to represent the Solidatus model
model = {
    entities: entities,
    transitions: {},
    roots: ['layer']
}

// Create a JavaScript object representing a ReplaceModel command using the model above
command = {
    model: model,
    cmd: 'ReplaceModel',
    comparator: {
        name: true
    }
}

// Build a request containing the command above
request = {
    commit: true,
    cmds: [command]
}

// Create the headers for our API requests
headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + argv.token
}

// Find a model using a search term
fetch(argv.host + '/api/v1/models/?searchTerm=' + encodeURI(argv.model), {
    method: 'get',
    headers: headers
}).then(function (response) {
    if (!response.ok) {
        throw Error(response.statusText)
    }
    return response.json()
}).then(function (json) {
    if (json.items.length > 0){
        return json.items[0]
    } else {
        // No model can be found, Create a new model with an API request
        return fetch(argv.host + '/api/v1/models', {
            method: 'post',
            headers: headers,
            body: JSON.stringify({name: argv.model})
        }).then(function (response) {
            if (!response.ok) {
                throw Error(response.statusText)
            }
          return response.json()
        }).catch(function (error) {
            console.error(error)
        })
    }
}).catch(function (error) {
    console.error(error)
})
// Then use the request created earlier to replace the contents of the found/created model with the model built earlier, getting the ID of the new model from the response JSON of the create model request
.then(function (responseJson) {
    fetch(argv.host + '/api/v1/models/' + responseJson.id + '/update', {
        method: 'post',
        headers: headers,
        body: JSON.stringify(request)
    }).then(function (response) {
        if (!response.ok) {
            throw Error(response.statusText)
        }
    }).catch(function (error) {
        console.error(error)
    })
})
