# CSV Loader
This is an example piece of JavaScript code to parse a .csv file and extract the structure to a new Solidatus model. It demonstrates how to build requests to interact with the Solidatus API to create and update a Solidatus model.

All code is contained within the `index.js` file.

## Requirements
* node
* npm
* a valid Solidatus API token, with Create Model permissions

To authenticate against the API, you will need to request an API token from your user account page in Solidatus. Information on how to get this can be found at `/help/api/authenticating.html` in your Solidatus instance help pages.

## Setup
This loader requires the use of 5 JavaScript packages. These can be installed using npm with the following commands:

* `npm i lodash`
* `npm i yargs`
* `npm i fs`
* `npm i csv-parse`
* `npm i node-fetch`


## How to run
This loader can be run with the command:

`node index.js load --in <input csv file> --host <Solidatus URL> --token <token> --model "<model name>"`

replacing:

* <input csv file> with the path to the csv file that you want to model
* <Solidatus URL> with the URL of the Solidatus instance that you are using, e.g. https://demo.solidatus.com
* <token> with your Solidatus API token
* <model name> with the name of the model that you want to create or update.
